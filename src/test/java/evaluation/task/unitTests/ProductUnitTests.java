package evaluation.task.unitTests;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import evaluation.task.controller.ProductController;
import evaluation.task.dao.ProductRepository;
import evaluation.task.dto.Product;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ProductUnitTests {
	
	MockMvc mockMvc;
	
	@MockBean
    private ProductRepository repositoryMock;
    
    @Autowired
    ProductController productController;
    
    private List<Product> products;
    Product product;
    
    
    @Before
    public void setup() throws Exception {
        this.mockMvc = standaloneSetup(this.productController).build();

        Product product = new Product();
        product.setBarcode(1L);
        product.setName("TestName");
        product.setBasePrice(new BigDecimal(2.5));
        product.setDescription("Test description");
        product.setReleaseDate("01.01.2000");
        
        products = new ArrayList<>();
        products.add(product);
        this.product = product;
    }
    
    @Test
    public void testGetAll() throws Exception {
        when(repositoryMock.findAll()).thenReturn(products);
         
    	mockMvc.perform(get("/getProducts"))
    	.andExpect(status().isOk())
    	.andExpect(jsonPath("$", hasSize(1)))
    	.andExpect(jsonPath("$[0].barcode", is(1)))
    	.andExpect(jsonPath("$[0].name", is("TestName")))
    	.andExpect(jsonPath("$[0].basePrice", is(2.5)))
    	.andExpect(jsonPath("$[0].description", is("Test description")))
    	.andExpect(jsonPath("$[0].releaseDate", is("01.01.2000")));
    	verify(repositoryMock, times(1)).findAll();
        verifyNoMoreInteractions(repositoryMock);
    }
    
    @Test
    public void testGetOne() throws Exception {
    	when(repositoryMock.findById(1L)).thenReturn(Optional.of(product));
    	mockMvc.perform(get("/products/{id}", 1L))
    	.andExpect(status().isOk())
    	.andExpect(jsonPath("$.barcode", is(1)))
    	.andExpect(jsonPath("$.name", is("TestName")))
    	.andExpect(jsonPath("$.basePrice", is(2.5)))
    	.andExpect(jsonPath("$.description", is("Test description")))
    	.andExpect(jsonPath("$.releaseDate", is("01.01.2000")));
    	verify(repositoryMock, times(1)).findById(1L);
        verifyNoMoreInteractions(repositoryMock);
    }
    
    @Test
    public void testNewProduct() throws Exception {
    	when(repositoryMock.save(product)).thenReturn(product);
    	ObjectMapper mapper = new ObjectMapper();
    	String productJson = mapper.writeValueAsString(product);
    	mockMvc.perform(post("/addProduct")
    	.contentType(MediaType.APPLICATION_JSON)
        .content(productJson)
        )
    	.andExpect(status().isCreated());
    	verify(repositoryMock, times(1)).save(any());
        verifyNoMoreInteractions(repositoryMock);
    }
    
    @Test
    public void testReplaceProduct() throws Exception {
    	when(repositoryMock.findById(1L)).thenReturn(Optional.of(product));
    	ObjectMapper mapper = new ObjectMapper();
    	String productJson = mapper.writeValueAsString(product);
    	mockMvc.perform(put("/products/{id}", 1L)
    	.contentType(MediaType.APPLICATION_JSON)
        .content(productJson)
        )
    	.andExpect(status().isOk());
    	verify(repositoryMock, times(1)).findById(1L);
    	verify(repositoryMock, times(1)).save(product);
        verifyNoMoreInteractions(repositoryMock);
    }
    
    @Test
    public void testDeleteProduct() throws Exception {
    	doNothing().when(repositoryMock).deleteById(1L);
    	mockMvc.perform(delete("/products/{id}", 1L))
    	.andExpect(status().isOk());
    	verify(repositoryMock, times(1)).deleteById(1L);
        verifyNoMoreInteractions(repositoryMock);
    }
}
