package evaluation.task.unitTests;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import evaluation.task.controller.ClientController;
import evaluation.task.dao.ClientRepository;
import evaluation.task.dto.Client;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ClientUnitTests {
	
	MockMvc mockMvc;
	
	@MockBean
    private ClientRepository repositoryMock;
    
    @Autowired
    ClientController clientController;
    
    private List<Client> clients;
    Client client;
    
    
    @Before
    public void setup() throws Exception {
        this.mockMvc = standaloneSetup(this.clientController).build();

        Client client = new Client();
        client.setSecurityNumber(1L);
        client.setFirstName("TestFirst");
        client.setLastName("TestLast");
        client.setPhone("55556666");
        client.setCountry("Test country");
        client.setAddress("Test address");
        
        clients = new ArrayList<>();
        clients.add(client);
        this.client = client;
    }
    
    @Test
    public void testGetAll() throws Exception {
        when(repositoryMock.findAll()).thenReturn(clients);
         
    	mockMvc.perform(get("/getClients"))
    	.andExpect(status().isOk())
    	.andExpect(jsonPath("$", hasSize(1)))
    	.andExpect(jsonPath("$[0].securityNumber", is(1)))
    	.andExpect(jsonPath("$[0].firstName", is("TestFirst")))
    	.andExpect(jsonPath("$[0].lastName", is("TestLast")))
    	.andExpect(jsonPath("$[0].phone", is("55556666")))
    	.andExpect(jsonPath("$[0].country", is("Test country")))
    	.andExpect(jsonPath("$[0].address", is("Test address")));
    	verify(repositoryMock, times(1)).findAll();
        verifyNoMoreInteractions(repositoryMock);
    }
    
    @Test
    public void testGetOne() throws Exception {
    	when(repositoryMock.findById(1L)).thenReturn(Optional.of(client));
    	mockMvc.perform(get("/clients/{id}", 1L))
    	.andExpect(status().isOk())
    	.andExpect(jsonPath("$.securityNumber", is(1)))
    	.andExpect(jsonPath("$.firstName", is("TestFirst")))
    	.andExpect(jsonPath("$.lastName", is("TestLast")))
    	.andExpect(jsonPath("$.phone", is("55556666")))
    	.andExpect(jsonPath("$.country", is("Test country")))
    	.andExpect(jsonPath("$.address", is("Test address")));
    	verify(repositoryMock, times(1)).findById(1L);
        verifyNoMoreInteractions(repositoryMock);
    }
    
    @Test
    public void testNewClient() throws Exception {
    	when(repositoryMock.save(client)).thenReturn(client);
    	ObjectMapper mapper = new ObjectMapper();
    	String clientJson = mapper.writeValueAsString(client);
    	mockMvc.perform(post("/addClient")
    	.contentType(MediaType.APPLICATION_JSON)
        .content(clientJson)
        )
    	.andExpect(status().isCreated());
    	verify(repositoryMock, times(1)).save(any());
        verifyNoMoreInteractions(repositoryMock);
    }
    
    @Test
    public void testReplaceClient() throws Exception {
    	when(repositoryMock.findById(1L)).thenReturn(Optional.of(client));
    	ObjectMapper mapper = new ObjectMapper();
    	String clientJson = mapper.writeValueAsString(client);
    	mockMvc.perform(put("/clients/{id}", 1L)
    	.contentType(MediaType.APPLICATION_JSON)
        .content(clientJson)
        )
    	.andExpect(status().isOk());
    	verify(repositoryMock, times(1)).findById(1L);
    	verify(repositoryMock, times(1)).save(client);
        verifyNoMoreInteractions(repositoryMock);
    }
    
    @Test
    public void testDeleteClient() throws Exception {
    	doNothing().when(repositoryMock).deleteById(1L);
    	mockMvc.perform(delete("/clients/{id}", 1L))
    	.andExpect(status().isOk());
    	verify(repositoryMock, times(1)).deleteById(1L);
        verifyNoMoreInteractions(repositoryMock);
    }
}
