package evaluation.task.integrationTests;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import evaluation.task.controller.ClientController;
import evaluation.task.dao.ClientRepository;
import evaluation.task.dto.Client;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class ClientIntegrationTests {
	@Autowired
	MockMvc mockMvc;
	
    @Autowired
    ClientController clientController;
    
    @Autowired
    private ClientRepository repository;
    
    @Before
    public void setup() throws Exception {
    	this.mockMvc = standaloneSetup(this.clientController).build();
    	Client client = new Client();
        client.setSecurityNumber(1L);
        client.setFirstName("TestFirst");
        client.setLastName("TestLast");
        client.setPhone("55556666");
        client.setCountry("Test country");
        client.setAddress("Test address");
        repository.save(client);
    }
    
    @After
    public void tearDown() throws Exception {
    	repository.deleteAll();
    }
    
    @Test
    public void testGetAll() throws Exception {
    	mockMvc.perform(get("/getClients"))
    	.andExpect(status().isOk())
    	.andExpect(jsonPath("$", hasSize(1)))
    	.andExpect(jsonPath("$[0].securityNumber", is(1)))
    	.andExpect(jsonPath("$[0].firstName", is("TestFirst")))
    	.andExpect(jsonPath("$[0].lastName", is("TestLast")))
    	.andExpect(jsonPath("$[0].phone", is("55556666")))
    	.andExpect(jsonPath("$[0].country", is("Test country")))
    	.andExpect(jsonPath("$[0].address", is("Test address")));
    }
    
    @Test
    public void testGetOne() throws Exception {
    	mockMvc.perform(get("/clients/{id}", 1L))
    	.andExpect(status().isOk())
    	.andExpect(jsonPath("$.securityNumber", is(1)))
    	.andExpect(jsonPath("$.firstName", is("TestFirst")))
    	.andExpect(jsonPath("$.lastName", is("TestLast")))
    	.andExpect(jsonPath("$.phone", is("55556666")))
    	.andExpect(jsonPath("$.country", is("Test country")))
    	.andExpect(jsonPath("$.address", is("Test address")));
    }
    
    @Test
    public void testNewClient() throws Exception {
    	Client newClient = new Client();
    	newClient.setSecurityNumber(2L);
    	newClient.setFirstName("NewFirst");
    	newClient.setLastName("NewLast");
    	newClient.setPhone("123123");
    	newClient.setCountry("New country");
    	newClient.setAddress("New address");
    	
    	ObjectMapper mapper = new ObjectMapper();
    	String newClientJson = mapper.writeValueAsString(newClient);
    	mockMvc.perform(post("/addClient")
    			.contentType(MediaType.APPLICATION_JSON)
    			.content(newClientJson)
        )
    	.andExpect(status().isCreated())
    	.andExpect(jsonPath("$.securityNumber", is(2)))
    	.andExpect(jsonPath("$.firstName", is("NewFirst")))
    	.andExpect(jsonPath("$.lastName", is("NewLast")))
    	.andExpect(jsonPath("$.phone", is("123123")))
    	.andExpect(jsonPath("$.country", is("New country")))
    	.andExpect(jsonPath("$.address", is("New address")));
    }
    
    @Test
    public void testReplaceClient() throws Exception {
    	Client newClient = new Client();
    	newClient.setSecurityNumber(1L);
    	newClient.setFirstName("NewFirst");
    	newClient.setLastName("NewLast");
    	newClient.setPhone("123123");
    	newClient.setCountry("New country");
    	newClient.setAddress("New address");
    	
    	ObjectMapper mapper = new ObjectMapper();
    	String newClientJson = mapper.writeValueAsString(newClient);
    	mockMvc.perform(put("/clients/{id}", 1L)
    	    	.contentType(MediaType.APPLICATION_JSON)
    	        .content(newClientJson)
    	        )
    	.andExpect(status().isOk())
    	.andExpect(jsonPath("$.securityNumber", is(1)))
    	.andExpect(jsonPath("$.firstName", is("NewFirst")))
    	.andExpect(jsonPath("$.lastName", is("NewLast")))
    	.andExpect(jsonPath("$.phone", is("123123")))
    	.andExpect(jsonPath("$.country", is("New country")))
    	.andExpect(jsonPath("$.address", is("New address")));
    }
    
    @Test
    public void testDeleteClient() throws Exception {
    	mockMvc.perform(delete("/clients/{id}", 1L))
    	.andExpect(status().isOk());
    }
}
