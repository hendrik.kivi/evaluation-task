package evaluation.task.integrationTests;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.math.BigDecimal;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import evaluation.task.controller.ProductController;
import evaluation.task.dao.ProductRepository;
import evaluation.task.dto.Product;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class ProductIntegrationTests {
	@Autowired
	MockMvc mockMvc;
	
    @Autowired
    ProductController productController;
    
    @Autowired
    private ProductRepository repository;
    
    @Before
    public void setup() throws Exception {
    	this.mockMvc = standaloneSetup(this.productController).build();
    	Product product = new Product();
        product.setBarcode(1L);
        product.setName("TestName");
        product.setBasePrice(new BigDecimal(2.5));
        product.setDescription("Test description");
        product.setReleaseDate("01.01.2000");
        repository.save(product);
    }
    
    @After
    public void tearDown() throws Exception {
    	repository.deleteAll();
    }
    
    @Test
    public void testGetAll() throws Exception {
    	mockMvc.perform(get("/getProducts"))
    	.andExpect(status().isOk())
    	.andExpect(jsonPath("$", hasSize(1)))
    	.andExpect(jsonPath("$[0].barcode", is(1)))
    	.andExpect(jsonPath("$[0].name", is("TestName")))
    	.andExpect(jsonPath("$[0].basePrice", is(2.5)))
    	.andExpect(jsonPath("$[0].description", is("Test description")))
    	.andExpect(jsonPath("$[0].releaseDate", is("01.01.2000")));
    }
    
    @Test
    public void testGetOne() throws Exception {
    	mockMvc.perform(get("/products/{id}", 1L))
    	.andExpect(status().isOk())
    	.andExpect(jsonPath("$.barcode", is(1)))
    	.andExpect(jsonPath("$.name", is("TestName")))
    	.andExpect(jsonPath("$.basePrice", is(2.5)))
    	.andExpect(jsonPath("$.description", is("Test description")))
    	.andExpect(jsonPath("$.releaseDate", is("01.01.2000")));
    }
    
    @Test
    public void testNewProduct() throws Exception {
    	Product newProduct = new Product();
    	newProduct.setBarcode(2L);
    	newProduct.setName("NewName");
    	newProduct.setBasePrice(new BigDecimal(1.5));
    	newProduct.setDescription("New description");
    	newProduct.setReleaseDate("01.01.2001");
    	
    	ObjectMapper mapper = new ObjectMapper();
    	String newProductJson = mapper.writeValueAsString(newProduct);
    	mockMvc.perform(post("/addProduct")
    			.contentType(MediaType.APPLICATION_JSON)
    			.content(newProductJson)
        )
    	.andExpect(status().isCreated())
    	.andExpect(jsonPath("$.barcode", is(2)))
    	.andExpect(jsonPath("$.name", is("NewName")))
    	.andExpect(jsonPath("$.basePrice", is(1.5)))
    	.andExpect(jsonPath("$.description", is("New description")))
    	.andExpect(jsonPath("$.releaseDate", is("01.01.2001")));
    }
    
    @Test
    public void testReplaceProduct() throws Exception {
    	Product newProduct = new Product();
    	newProduct.setBarcode(2L);
    	newProduct.setName("NewName");
    	newProduct.setBasePrice(new BigDecimal(1.5));
    	newProduct.setDescription("New description");
    	newProduct.setReleaseDate("01.01.2001");    	
    	
    	ObjectMapper mapper = new ObjectMapper();
    	String newProductJson = mapper.writeValueAsString(newProduct);
    	mockMvc.perform(put("/products/{id}", 1L)
    	    	.contentType(MediaType.APPLICATION_JSON)
    	        .content(newProductJson)
    	        )
    	.andExpect(status().isOk())
    	.andExpect(jsonPath("$.barcode", is(1)))
    	.andExpect(jsonPath("$.name", is("NewName")))
    	.andExpect(jsonPath("$.basePrice", is(1.5)))
    	.andExpect(jsonPath("$.description", is("New description")))
    	.andExpect(jsonPath("$.releaseDate", is("01.01.2001")));
    }
    
}
