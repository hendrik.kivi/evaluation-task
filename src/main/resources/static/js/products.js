App.controller('ProductsController', function($scope, $http) {
	$scope.productForm = {
        barcode: 0,
		name: "",
        basePrice: 0.0,
        description: "",
        releaseDate:new Date(new Date().getFullYear(),new Date().getMonth() , new Date().getDate()),
    };
	refreshData();
	
	$scope.saveProduct = function() {
		if (validateForm()) {
			$http.get('http://localhost:8080/products/' + $scope.productForm.barcode).
	    	then(function(response) {
	    		editProduct();
	    	}, function(err) {
	    		addProduct();
	    	});
		}
	}
	
	$scope.editForm = function(product) {
		console.log(product.releaseDate)
		$scope.productForm.barcode = product.barcode;
		$scope.productForm.name = product.name;
        $scope.productForm.basePrice = product.basePrice;
        $scope.productForm.description = product.description
        $scope.productForm.releaseDate = new Date(product.releaseDate);
	}
	
	$scope.deleteProduct = function(product) {
		$http.delete('http://localhost:8080/products/' + product.barcode)
		.then(function(response) {
			refreshData();
			clearForm();
		});
	}
	
	function addProduct() {
		$http.post('http://localhost:8080/addProduct', angular.toJson($scope.productForm))
			.then(function(response) {
				refreshData();
				clearForm();
			});
	}
	
	function editProduct() {
		$http.put('http://localhost:8080/products/' + $scope.productForm.barcode, 
				angular.toJson($scope.productForm))
			.then(function(response) {
				refreshData();
				clearForm();
			});
	}
	
	function refreshData() {
		$http.get('http://localhost:8080/getProducts').
        then(function(response) {
            $scope.products = response.data;
        });
	}
	
	function clearForm() {
		$scope.productForm.barcode = 0;
        $scope.productForm.name = "";
        $scope.productForm.basePrice = 0;
        $scope.productForm.description = "";
        $scope.productForm.releaseDate = new Date(new Date().getFullYear(),new Date().getMonth() , new Date().getDate());
	}
	
	function validateForm() {
		if ($scope.productForm.barcode == null ||
				$scope.productForm.name == "" ||
				$scope.productForm.basePrice == null) {
			return false;
		} else {
			return true;
		}
	}
});