App.controller('ClientsController', function($scope, $http) {
	$scope.clientForm = {
        securityNumber: 0,
		firstName: "",
        lastName: "",
        phone: "",
        country: "",
        address: ""
    };
	
	refreshData();
	
	$scope.saveClient = function() {
		if (validateForm()) {
			$http.get('http://localhost:8080/clients/' + $scope.clientForm.securityNumber).
	    	then(function(response) {
	    		editClient();
	    	}, function(err) {
	    		addClient();
	    	});
		}
	}
	
	$scope.editForm = function(client) {
		$scope.clientForm.securityNumber = client.securityNumber;
		$scope.clientForm.firstName = client.firstName;
        $scope.clientForm.lastName = client.lastName;
        $scope.clientForm.phone = client.phone
        $scope.clientForm.country = client.country;
        $scope.clientForm.address = client.address
	}
	
	$scope.deleteClient = function(client) {
		$http.delete('http://localhost:8080/clients/' + client.securityNumber)
		.then(function(response) {
			refreshData();
			clearForm();
		});
	}
	
	function addClient() {
		$http.post('http://localhost:8080/addClient', angular.toJson($scope.clientForm))
			.then(function(response) {
				refreshData();
				clearForm();
			});
	}
	
	function editClient() {
		$http.put('http://localhost:8080/clients/' + $scope.clientForm.securityNumber, 
				angular.toJson($scope.clientForm))
			.then(function(response) {
				refreshData();
				clearForm();
			});
	}
	
	function refreshData() {
		$http.get('http://localhost:8080/getClients').
        then(function(response) {
            $scope.clients = response.data;
        });
	}
	
	function clearForm() {
		$scope.clientForm.securityNumber = 0;
		$scope.clientForm.firstName = "";
        $scope.clientForm.lastName = "";
        $scope.clientForm.phone = "";
        $scope.clientForm.country = "";
        $scope.clientForm.address = "";
	}
	
	function validateForm() {
		if ($scope.clientForm.securityNumber == null ||
				$scope.clientForm.firstName == "" ||
				$scope.clientForm.lastName == "") {
			return false;
		} else {
			return true;
		}
	}
	
});