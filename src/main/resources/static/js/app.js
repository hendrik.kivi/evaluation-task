var App = angular.module('myApp',['ngRoute']);
 
App.config(['$routeProvider', function($routeProvider) {
    $routeProvider
    	.when('/clients', {
            templateUrl: 'clients',
            controller : "ClientsController",
        })
        .when('/products', {
            templateUrl: 'products',
            controller : "ProductsController",
        })
        .when('/orders', {
            templateUrl: 'orders',
            controller : "OrdersController",
        })
        .otherwise({redirectTo:'/clients'});
}]);