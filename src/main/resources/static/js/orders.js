App.controller('OrdersController', function($scope, $http) {
	$scope.orderForm = {
        orderNumber: "",
        transactionDate: new Date(new Date().getFullYear(),new Date().getMonth() , new Date().getDate()),
        barcode: "",
        clientId: ""
    };
	$scope.sortedBy = ['transactionDate','product.name'];
	$scope.reverse = false;
	
	refreshData();
	
	$scope.saveOrder = function() {
		if (validateForm()) {
			if ($scope.orderForm.orderNumber == "") {
				formToOrder($scope.orderForm, function(order) {
					addOrder(order)
				});
	        } else {
	        	formToOrder($scope.orderForm, function(order) {
	        		editOrder(order);
				});
	        }
		}
	}
	
	$scope.sortBy = function(propertyName) {
		$scope.reverse = ($scope.sortedBy === propertyName) ? !$scope.reverse : false;
	    $scope.sortedBy = propertyName;
	};
	
	$scope.editForm = function(order) {
		$scope.orderForm.orderNumber = order.orderNumber;
		$scope.orderForm.price = order.price;
        $scope.orderForm.transactionDate = new Date(order.transactionDate);
        $scope.orderForm.barcode = order.product.barcode
        $scope.orderForm.clientId = order.client.securityNumber
	}
	
	$scope.deleteOrder = function(order) {
		$http.delete('http://localhost:8080/orders/' + order.orderNumber)
		.then(function(response) {
			refreshData();
			clearForm();
		});
	}
	
	function addOrder(order) {
		$http.post('http://localhost:8080/addOrder', angular.toJson(order))
			.then(function(response) {
				refreshData();
				clearForm();
			});
	}
	
	function editOrder(order) {
		$http.put('http://localhost:8080/orders/' + order.orderNumber, 
				angular.toJson(order))
			.then(function(response) {
				refreshData();
				clearForm();
			});
	}
	
	function refreshData() {
		$http.get('http://localhost:8080/getOrders').
        then(function(response) {
            $scope.orders = response.data;
        });
	}
	
	function clearForm() {
		$scope.orderForm.orderNumber = "";
        $scope.orderForm.price = "";
        $scope.orderForm.transactionDate = new Date(new Date().getFullYear(),new Date().getMonth() , new Date().getDate());
        $scope.orderForm.barcode = "";
        $scope.orderForm.clientId = "";
	}
	
	function formToOrder(orderForm, callback) {
		$http.get('http://localhost:8080/products/' + orderForm.barcode).
        then(function(productResponse) {
        	$http.get('http://localhost:8080/clients/' + orderForm.clientId).
            then(function(clientResponse) {
                var order = {
        			orderNumber: orderForm.orderNumber,
        			price: productResponse.data.basePrice,
        			transactionDate: orderForm.transactionDate,
        			product: productResponse.data,
        			client: clientResponse.data
        		};
                $scope.errorMessage = false;
                callback(order);
            }, function(err) {
            	$scope.errorMessage = 'No client with security number ' + orderForm.clientId;
            });
        }, function(err) {
        	$scope.errorMessage = 'No product with barcode ' + orderForm.barcode;
        });
	}
	
	function validateForm() {
		if ($scope.orderForm.transactionDate == null ||
				$scope.orderForm.barcode == null ||
				$scope.orderForm.clientId == null) {
			return false;
		} else {
			return true;
		}
	}
	
});