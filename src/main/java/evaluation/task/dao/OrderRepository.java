package evaluation.task.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import evaluation.task.dto.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long>{

}