package evaluation.task.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import evaluation.task.dto.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long>{

}
