package evaluation.task.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import evaluation.task.dto.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long>{

}
