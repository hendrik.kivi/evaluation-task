package evaluation.task.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import evaluation.task.dao.OrderRepository;
import evaluation.task.dto.Order;

@RestController
public class OrderController {
	@Autowired
	private OrderRepository repository;
	
	@GetMapping("/getOrders")
	public ResponseEntity<List<Order>> getAll() {
		List<Order> orders = repository.findAll();
		return new ResponseEntity<List<Order>>(orders, HttpStatus.OK);
	}
	
	@PostMapping("/addOrder")
	public ResponseEntity<Order> newOrder(@RequestBody Order newOrder) {
		repository.save(newOrder);
		return new ResponseEntity<Order>(newOrder, HttpStatus.CREATED);
	}
	
	@GetMapping("/orders/{id}")
	public ResponseEntity<Order> getOne(@PathVariable Long id) {
		Optional<Order> order = repository.findById(id);
		if (order.isPresent()) {
			return new ResponseEntity<Order>(order.get(), HttpStatus.OK);
		}
		else {
			 return new ResponseEntity<Order>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PutMapping("/orders/{id}")
	public ResponseEntity<Order> replaceOrder(@RequestBody Order newOrder, @PathVariable Long id) {
		return repository.findById(id)
			.map(order -> {
				order.setPrice(newOrder.getPrice());
				order.setTransactionDate(newOrder.getTransactionDate());
				order.setProduct(newOrder.getProduct());
				order.setClient(newOrder.getClient());
				repository.save(order);
				return new ResponseEntity<Order>(order, HttpStatus.OK);
			})
			.orElseGet(() -> {
				return new ResponseEntity<Order>(HttpStatus.NOT_FOUND);
			});
	}
	
	@DeleteMapping("/orders/{id}")
	public ResponseEntity<Void> deleteOrder(@PathVariable Long id) {
		repository.deleteById(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
}
