package evaluation.task.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ViewController {
	@RequestMapping("/clients")
	public String clients() {
		return "clients";
	}
	
	@RequestMapping("/products")
	public String products() {
		return "products";
	}
	
	@RequestMapping("/orders")
	public String orders() {
		return "orders";
	}
}
