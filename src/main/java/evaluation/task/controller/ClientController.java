package evaluation.task.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import evaluation.task.dao.ClientRepository;
import evaluation.task.dto.Client;

@RestController
public class ClientController {

	@Autowired
	private ClientRepository repository;
	
	@GetMapping("/getClients")
	public ResponseEntity<List<Client>> getAll() {
		List<Client> clients = repository.findAll();
		return new ResponseEntity<List<Client>>(clients, HttpStatus.OK);
	}
	
	@PostMapping("/addClient")
	public ResponseEntity<Client> newClient(@RequestBody Client newClient) {
		repository.save(newClient);
		return new ResponseEntity<Client>(newClient, HttpStatus.CREATED);
	}
	
	@GetMapping("/clients/{id}")
	public ResponseEntity<Client> getOne(@PathVariable Long id) {
		Optional<Client> client = repository.findById(id);
		if (client.isPresent()) {
			return new ResponseEntity<Client>(client.get(), HttpStatus.OK);
		}
		else {
			 return new ResponseEntity<Client>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PutMapping("/clients/{id}")
	public ResponseEntity<Client> replaceClient(@RequestBody Client newClient, @PathVariable Long id) {
		return repository.findById(id)
			.map(client -> {
				client.setFirstName(newClient.getFirstName());
				client.setLastName(newClient.getLastName());
				client.setPhone(newClient.getPhone());
				client.setCountry(newClient.getCountry());
				client.setAddress(newClient.getAddress());
				repository.save(client);
				return new ResponseEntity<Client>(client, HttpStatus.OK);
			})
			.orElseGet(() -> {
				return new ResponseEntity<Client>(HttpStatus.NOT_FOUND);
			});
	}
	
	@DeleteMapping("/clients/{id}")
	public ResponseEntity<Void> deleteClient(@PathVariable Long id) {
		repository.deleteById(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
}
