package evaluation.task.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import evaluation.task.dao.ProductRepository;
import evaluation.task.dto.Product;


@RestController
public class ProductController {
	@Autowired
	private ProductRepository repository;
	
	@GetMapping("/getProducts")
	public ResponseEntity<List<Product>> getAll() {
		List<Product> products = repository.findAll();
		return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
	}
	
	@PostMapping("/addProduct")
	public ResponseEntity<Product> newProduct(@RequestBody Product newProduct) {
		repository.save(newProduct);
		return new ResponseEntity<Product>(newProduct, HttpStatus.CREATED);
	}
	
	@GetMapping("/products/{id}")
	public ResponseEntity<Product> getOne(@PathVariable Long id) {
		Optional<Product> product = repository.findById(id);
		if (product.isPresent()) {
			return new ResponseEntity<Product>(product.get(), HttpStatus.OK);
		}
		else {
			 return new ResponseEntity<Product>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PutMapping("/products/{id}")
	public ResponseEntity<Product> replaceProduct(@RequestBody Product newProduct, @PathVariable Long id) {
		return repository.findById(id)
			.map(product -> {
				product.setName(newProduct.getName());
				product.setBasePrice(newProduct.getBasePrice());
				product.setDescription(newProduct.getDescription());
				product.setReleaseDate(newProduct.getReleaseDate());
				repository.save(product);
				return new ResponseEntity<Product>(product, HttpStatus.OK);
			})
			.orElseGet(() -> {
				return new ResponseEntity<Product>(HttpStatus.NOT_FOUND);
			});
	}
	
	@DeleteMapping("/products/{id}")
	public ResponseEntity<Product> deleteProduct(@PathVariable Long id) {
		repository.deleteById(id);
		return new ResponseEntity<Product>(HttpStatus.OK);
	}
}
