package evaluation.task.dto;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "product_table")
public class Product {
	@Id
	private Long barcode;
	private String name;
	private BigDecimal basePrice;
	private String description;
	private String releaseDate;
	
}
