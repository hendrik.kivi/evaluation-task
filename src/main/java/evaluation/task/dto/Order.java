package evaluation.task.dto;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "order_table")
public class Order {
	@Id
	@GeneratedValue
	private Long orderNumber;
	private BigDecimal price;
	private String transactionDate;
	@ManyToOne
    @JoinColumn(name="product_table")
    private Product product;
	@ManyToOne
    @JoinColumn(name="client_table")
    private Client client;
}
