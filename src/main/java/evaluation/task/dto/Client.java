package evaluation.task.dto;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "client_table")
public class Client {
	@Id
	private Long securityNumber;
	private String firstName;
	private String lastName;
	private String phone;
	private String country;
	private String address;
}
