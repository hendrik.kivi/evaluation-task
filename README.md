### Deployment
1. `gradlew build` to build
2. `java -jar build/libs/evaluation-task-0.1.0.jar` to deploy
3. Start the application at http://localhost:8080/